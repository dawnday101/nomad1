
job "wordpress" {
  datacenters = ["solute1"]
  group "wordpress" {
    # Scale count to handle your expected traffic
    count = 3

    task "wordpress" {
      driver = "docker"
      config {
        image = "wordpress:wordpress1"
        port_map {
          http = 80
        }
      }

      resources {
        cpu    = 500 # 500 MHz
        memory = 256 # 256MB
        network {
          mbits = 10
          port "http" {
            static = "8060"
          }
        }
      }

      service {
        name = "wordpress"
        port = "http"
        check {
          name     = "alive"
          type     = "http"
          interval = "10s"
          timeout  = "2s"
        }
      }

      # env = true requires Nomad  0.4
      template {
        data          = <<EOF
WORDPRESS_DB_HOST={{ key "service/mysql" }}
{{ with secret "database/creds/mysql" }}
WORDPRESS_DB_USER={{ .Data.username }}
WORDPRESS_DB_PASSWORD={{ .Data.password }}
EOF
        change_mode   = "restart"
        env           = true
      }

      vault {
        policies      = ["database"]
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }
    }
  }
}